import React from "react";

import classes from "./UsersList.module.css";
import deleteBtn from "../assets/delete.svg";

const UsersList = ({ usersList, deleteUser }) => {
  return (
    <div className={classes.users}>
      <div className={classes.title}>Users</div>
      <div className={classes.line}></div>
      <div className={classes.names}>
        <div className={classes.item}>№</div>
        <div className={classes.item}>Full name</div>
        <div className={classes.item}>Date of Birth</div>
        <div className={classes.item}>Phone</div>
        <div className={classes.item}>Email</div>
        <div className={classes.item}>Company Name</div>
        <div className={classes.item}>Selected Job</div>
        <div className={classes.item}>Experience</div>
        <div className={classes.item}></div>
      </div>
      {usersList.map((user) => {
        return (
          <div className={classes.data} key={user?.uuid}>
            <div className={classes.item}>{usersList.indexOf(user) + 1}</div>
            <div className={classes.item}>
              {user?.user_infos.firstName} {user?.user_infos.lastName}
            </div>
            <div className={classes.item}>
              {user?.user_infos.dob.slice(0, 10)}
            </div>
            <div className={classes.item}>{user?.user_infos.phone_number}</div>
            <div className={classes.item}>{user?.user_infos.email}</div>
            <div className={classes.item}>{user?.work_area.company_name}</div>
            <div className={classes.item}>{user?.work_area.job_type}</div>
            <div className={classes.item}>{user?.work_area.experience}</div>
            <div
              className={classes.item}
              onClick={() => {
                deleteUser(user?.uuid);
              }}
            >
              <img src={deleteBtn} alt="delete" />
            </div>
          </div>
        );
      })}
    </div>
  );
};

export default UsersList;
