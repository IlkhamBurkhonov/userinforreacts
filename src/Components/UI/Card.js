import classes from "./Card.module.css";

const Card = (props) => {
  return (
    <div className={classes.card}>
      <div className={classes.title}>{props.title}</div>
      <div className={classes.line}></div>
      <div>{props.children}</div>
    </div>
  );
};

export default Card;
