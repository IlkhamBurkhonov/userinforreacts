import React from "react";

import classes from "./Input.module.css";

const Input = React.forwardRef((props, ref) => {
  return (
    <div className={classes.input}>
      <label htmlFor={props.input.id}>{props.label}</label>
      {props.select ? (
        <select
          ref={ref}
          name={props.id}
          id={props.id}
          defaultValue={"DEFAULT"}
        >
          <option value="DEFAULT" disabled>
            Select {props.label}
          </option>
          {props.jobType
            ? props.options.map((opt, i) => (
                <option value={opt.value} key={opt.id}>
                  {opt.label}
                </option>
              ))
            : props.options.map((opt, i) => (
                <option value={opt} key={i}>
                  {opt}
                </option>
              ))}
        </select>
      ) : (
        <input ref={ref} {...props.input} />
      )}
      {props.input.invalid === "true" ? (
        <p className={classes.invalid}>{props.label} is required</p>
      ) : (
        ""
      )}
    </div>
  );
});

export default Input;
