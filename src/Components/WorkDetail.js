import { useEffect } from "react";
import Card from "./UI/Card";
import Input from "./UI/Input";
import classes from "./WorkDetail.module.css";

const WorkDetail = ({
  companyNameRef,
  jobTypeRef,
  experienceRef,
  validity,
  jobTypes,
  getJobTypes,
}) => {
  useEffect(() => {
    getJobTypes();
  }, [getJobTypes]);

  return (
    <Card title={"Work Detail"}>
      <Input
        ref={companyNameRef}
        label="Company Name"
        input={{
          id: "companyName",
          type: "text",
          placeholder: "Enter your company name",
          invalid: `${!validity?.isCompanyValid}`,
        }}
      />
      <div className={classes.dropdown}>
        <Input
          ref={jobTypeRef}
          select
          options={jobTypes}
          jobType
          label="Job Type"
          input={{
            id: "jobType",
            invalid: `${!validity?.isJobTypeValid}`,
          }}
        />
        <Input
          ref={experienceRef}
          select
          options={[1, 2, 3, 4, 5]}
          label="Experience"
          input={{
            id: "experience",
            invalid: `${!validity?.isExperienceValid}`,
          }}
        />
      </div>
    </Card>
  );
};

export default WorkDetail;
