import Card from "./UI/Card";
import Input from "./UI/Input";

const UserInfo = ({
  nameInputRef,
  sNameInputRef,
  phoneNumberRef,
  emailInputRef,
  birthDateInputRef,
  validity,
}) => {
  return (
    <Card title={"User's Info"}>
      <Input
        ref={nameInputRef}
        label="Name"
        input={{
          id: "name",
          type: "text",
          placeholder: "Enter your first name",
          invalid: `${!validity?.isNameValid}`,
        }}
      />
      <Input
        ref={sNameInputRef}
        label="Surname"
        input={{
          id: "surname",
          type: "text",
          placeholder: "Enter your surname",
          invalid: `${!validity?.isSNameValid}`,
        }}
      />
      <Input
        ref={phoneNumberRef}
        label="Phone Number"
        input={{
          id: "phoneNumber",
          type: "text",
          placeholder: "Enter your phone number",
          invalid: `${!validity?.isPhoneNumberValid}`,
        }}
      />
      <Input
        ref={emailInputRef}
        label="Email"
        input={{
          id: "email",
          type: "email",
          placeholder: "Enter your email",
          invalid: `${!validity?.isEmailValid}`,
        }}
      />
      <Input
        ref={birthDateInputRef}
        label="Date of Birth"
        input={{
          id: "birthDate",
          type: "date",
          placeholder: "Enter your date of birth",
          invalid: `${!validity?.isBirthDateValid}`,
        }}
      />
    </Card>
  );
};

export default UserInfo;
