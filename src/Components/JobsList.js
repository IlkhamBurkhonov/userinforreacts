import React from "react";

import classes from "./JobsLists.module.css";
import deleteBtn from "../assets/delete.svg";

const JobLists = ({ jobTypes, deleteJobTypes }) => {
  return (
    <div className={classes.jobs}>
      <div className={classes.title}>Job type</div>
      <div className={classes.line}></div>
      <div className={classes.names}>
        <div className={classes.item}>№</div>
        <div className={classes.item}>Label</div>
        <div className={classes.item}></div>
      </div>
      {jobTypes.map((job) => (
        <div className={classes.data} key={job?.id}>
          <div className={classes.item}>{jobTypes.indexOf(job) + 1}</div>
          <div className={classes.item}>{job?.label}</div>
          <div
            className={classes.item}
            onClick={() => {
              deleteJobTypes(job?.id);
            }}
          >
            <img src={deleteBtn} alt="delete" />
          </div>
        </div>
      ))}
    </div>
  );
};

export default JobLists;
